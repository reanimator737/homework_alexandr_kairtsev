let timer = setInterval(func, 2000);
let buttonCancel = document.createElement('button');
buttonCancel.innerText = 'Прекратить';
let buttonRestart = document.createElement('button');
buttonRestart.innerText = 'Возобновить показ';
buttonRestart.classList.add('invis');
document.body.append(buttonCancel,buttonRestart);
buttonCancel.onclick = function (event) {
    clearTimeout(timer);
    buttonRestart.classList.remove('invis')
};
buttonRestart.onclick = function () {
    timer = setInterval(func, 2000);
    buttonRestart.classList.add('invis')
};
function func() {
    let active = document.querySelector('.active');
    let near = active.nextElementSibling;
    if (near === null){
        near = document.querySelectorAll('img')[0];
    }
    active.classList.remove('active');
    near.classList.add('active');
}
