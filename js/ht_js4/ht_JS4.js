function createNewUser(firstName, lastName){
    let newUser={};
    Object.defineProperty(newUser, 'firstName', {
        get: function() { return firstName; },
        set: function(newValue) { firstName = newValue; },
    });
    Object.defineProperty(newUser, 'lastName', {
        get: function() { return lastName; },
        set: function(newValue) { lastName = newValue; },
    });
    newUser.getLogin = function () {
        return firstName[0].toLowerCase() + lastName.toLowerCase();
    };
    return newUser;
}
let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'));