let value;
let input = document.createElement('input');
const main = document.querySelector('.main');
const  conteiner = document.querySelector('.conteiner');
const  price = document.querySelector('.price');
const decisionLet = document.getElementsByClassName('decision')[0];
input.type = 'number';
input.innerHTML = '';
main.append(input);
input.onfocus = function () {
    main.classList.add('onFocus');
    price.style.color = 'white';
    if (conteiner.lastElementChild.className === 'block'){
        conteiner.lastElementChild.remove();
        main.classList.remove('error-main');
   }
}
input.onblur = function () {
    decisionLet.innerHTML = ' ';
    price.style.color = 'black';
    main.classList.remove('onFocus');
    value = input.value;
    if (value < 0){
        error();
    } else  if (value >= 0 && value !== ''){
        decision(value);
    }
}
function error() {
    decisionLet.innerHTML = ''
    let error = document.createElement('div')
    price.style.color = 'white'
    error.className = 'block';
    main.classList.add('error-main');
    error.innerText = `Please enter correct price`;
    conteiner.append(error);
}
function decision(value) {
    let span = document.createElement('span');
    let cross = document.createElement('span');
    let div = document.createElement('div');
    span.innerText = `Current price: ${value}`;
    div.style.cssText = 'justify-content: space-between; display: flex; margin:0 5px';
    cross.innerText = 'X';
    cross.style.color = 'red';
    div.innerHTML = '';
    div.append(span, cross);
    decisionLet.append(div);
    cross.onclick = function () {
        div.remove()
    }
}