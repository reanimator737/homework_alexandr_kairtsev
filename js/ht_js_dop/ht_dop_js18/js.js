function deepclone(obj) {
    let clone = {};
    for (let key in obj) {
        if ("object"===typeof obj[key])
            clone[key] = deepclone(obj[key]);
        else
            clone[key] = obj[key];
    }
    return clone;
}
let test ={
    j1: 1,
    j2: '2',
    j3: {
        j3_1: 11,
        j3_2: '22'
    },
    j4: true
};
let clone = deepclone(test);