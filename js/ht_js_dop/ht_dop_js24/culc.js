let display = document.querySelector('.display input');
let sign;
let costSign;
let history = 0;
let counter = 0;
document.querySelector('.keys').onclick  = boxClick;
function boxClick(event) {
    let  value = event.target.value;
    if (event.target.tagName === 'INPUT'){
        if ('1234567890'.includes(value)){
            if (costSign){
                display.value += costSign + value;
                costSign = undefined;
            } else  {
                display.value += value;
            }
        }
        else if (value === 'C'){
            costSign = undefined;
            sign = undefined;
            display.value = '';
        }
        else if (value === '.'){
            checkThePoint()
            }
        else if(value ==='mrc'){
            if (document.querySelector('.display').children[1]){
                if (counter !== 1){
                    display.value = history;
                    sign = undefined;
                    costSign = undefined;
                    counter++
                } else {
                    document.querySelector('.display').children[1].remove();
                    history = 0;
                    counter = 0;
                }
            }
        }
        else if(value ==='m-' || value === 'm+'){
            addHistory (value)
        }
        else if ('+/-*'.includes(value)){
            if (display.value === '' && value === '-'){
                display.value += value;
            } else  if (display.value === '' || display.value === '-' ) {
                sign = undefined;
            } else if (sign && display.value.includes(sign,1)){
               culc()
            } else {
                costSign = value;
                sign = value;
            }
        }
        else if(value === '=' && sign && !costSign){
            culc()
        }
    }
}
function culc() {
   let index = display.value.indexOf(sign,1);
   let first = +display.value.slice(0, index);
   let second = +display.value.slice(++index);
   switch (sign) {
       case '+':
           display.value = first + second;
           break;
       case '-':
           display.value = first - second;
           break;
       case '*':
           display.value = first * second;
           break;
       case '/':
           display.value = first / second;
           break;
   }
}
function checkThePoint() {
    if (display.value !== '' && display.value !== '-'){
        if (display.value.includes(sign, 1) && !display.value.includes('.', display.value.indexOf(sign,1))) {
            display.value += '.'
           } else if(!display.value.includes('.')){
            display.value += '.'
        }
        } else {
        display.value += `0.`
    }
}
function addHistory(value) {
    let m = document.createElement('span');
    m.innerText = 'm';
    if (!document.querySelector('.display').children[1]){
        m.classList.add('memory');
        document.querySelector('.display').append(m);
    }
    let toHistory = 0;
    if(sign|| display.value.includes('.', display.value.indexOf(sign,1))){
        toHistory = +display.value.slice(display.value.indexOf(sign,1)+1)
    } else if (!sign || !display.value.includes(sign, 1)){
        toHistory = +display.value
    }
    switch (value) {
        case 'm+':
            counter = 0;
            history += toHistory;
            break;
        case 'm-':
            counter = 0;
            history -= toHistory;
            break;
    }
}
document.body.onkeypress = function (event) {
    console.log(event.key);
    switch (event.key) {
        case '1' :
            document.getElementById('1').click();
            break;
        case '2' :
            document.getElementById('2').click();
            break;
        case '3' :
            document.getElementById('3').click();
            break;
        case '4' :
            document.getElementById('4').click();
            break;
        case '5' :
            document.getElementById('5').click();
            break;
        case '6' :
            document.getElementById('6').click();
            break;
        case '7' :
            document.getElementById('7').click();
            break;
        case '8' :
            document.getElementById('8').click();
            break;
        case '9' :
            document.getElementById('9').click();
            break;
        case '0' :
            document.getElementById('0').click();
            break;
        case 'Enter' :
            document.getElementById('=').click();
            break;
        case '+' :
            document.getElementById('+').click();
            break;
        case '-' :
            document.getElementById('-').click();
            break;
        case '*' :
            document.getElementById('*').click();
            break;
        case '/' :
            document.getElementById('/').click();
            break;
        case '.' :
            document.getElementById('.').click();
            break;
    }
};