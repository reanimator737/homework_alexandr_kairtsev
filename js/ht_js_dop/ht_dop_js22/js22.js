let table = document.createElement('table');
createTable(table, 30, 30);
document.body.onclick = function (event) {
    if (event.target === this){
        document.body.classList.toggle('toChangeAll')
    }
};
table.onclick = function (event ) {
    if (event.target.tagName === 'TD'){
        event.target.classList.toggle('black');
        event.target.classList.toggle('white');
    }
};
function createTable(table,col,row){
    for (let i = 0;  i < row; i++){
        let tr = document.createElement('tr');
        for (let j = 0 ; j < col; j++){
            let td = document.createElement('td');
            td.className = 'white';
            tr.append(td)
        }
        table.append(tr)
    }
    document.body.append(table);
}
