let button = document.createElement('button');
const  container = document.querySelector('.conteiner');
button.innerText = 'Нарисовать круг';
let input = document.createElement('input');
input.onkeyup= function() {this.value = this.value.replace(/[^\d]/g,'')};
let div = document.createElement('div');
div.innerText = 'Введите диаметр';
button.onclick = function(event){
  if (container.children.length > 1){
    let value =  container.lastElementChild.value;
    if (value !== '' && +value[0] !== 0){
      createCircle(+value)
    } else {
      container.lastElementChild.value = '';
     alert('Введите данные корректно')
    }
  } else {
    container.append(div,input)
  }
};
container.append(button);

function generateColor() {
  let color = Math.floor(Math.random() * 16777216).toString(16);
  return '#000000'.slice(0, -color.length) + color;
}
function createCircle(value) {
  document.body.className = '';
  container.innerHTML = '';
  container.className = 'contentWithCircle';
  for (let i = 0; i !==100; i++){
    const circle= document.createElement('div');
    circle.className = 'circle';
    circle.style.cssText = `
    background: ${generateColor()};
    width: ${value - 2}px;
    height: ${value - 2}px;
    border-radius: ${value/2}px;
    `;
    container.append(circle);
  }
  container.onclick = function (event) {
    if (event.target !== this){
      let i = 0;
      let timer = setInterval(function () {
        event.target.style.background = generateColor();
        if (i === 15){
          event.target.style.display = 'none'
        }
        i++;
      },100);

    }
  }
}