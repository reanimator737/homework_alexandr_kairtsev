let title = document.getElementsByClassName('tabs')[0];
let content = document.getElementsByClassName('tabs-content')[0];
title.onclick = function (event) {
    clear()
    let i = 0;
    for (let key of title.children){
        if (event.target === key){
            chooseContent(i);
            key.classList.add('active');
            break
        }
        i++
    }
}
function clear() {
    for (let key  of content.children){
        key.className =  'unvisible'
    }
    for (let key of title.children){
        key.className =  'tabs-title'
    }
}
function chooseContent(numOfCell) {
    let li = content.children[numOfCell];
    li.className = 'visible'
}