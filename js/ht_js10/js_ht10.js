let firstIcon = document.getElementsByTagName('i')[0];
let secondIcon = document.getElementsByTagName('i')[1];
let firstInput  = document.getElementsByTagName('input')[0];
let secondInput  = document.getElementsByTagName('input')[1];
let labelWithError = document.getElementsByClassName('input-wrapper')[1];
secondInput.readOnly = true;
firstIcon.addEventListener('click', clickEvent);
secondIcon.addEventListener('click', clickEvent);
firstInput.addEventListener('click', deleteError);
secondInput.addEventListener('click', deleteError);
let button  = document.getElementsByTagName('button')[0];
button.onclick = function (event) {
    deleteError();
    if (firstInput.value  === secondInput.value && firstInput.value){
        alert('You are welcome')
    }
    else {
        let div = document.createElement('div');
        div.innerText = 'Нужно ввести одинаковые значения';
        div.style.cssText = 'background: red; color: white; margin-bottom: 20px; font-size: 15pt';
        labelWithError.append(div);
    }
};
function clickEvent(event){
    event.target.classList.toggle('fa-eye');
    event.target.classList.toggle('fa-eye-slash');
    let sisterEl = event.target.parentElement.children[0];
    if (sisterEl.type === 'password'){
        sisterEl.type = 'text'
    } else {
        sisterEl.type = 'password'
    }
}
firstInput.onblur = function (event) {
    if(firstInput.value){
        secondInput.readOnly = false;
    } else {
        secondInput.value  = '';
        secondInput.readOnly = true;
    }
};
document.getElementsByTagName('form')[0].onkeypress = function (event) {
    if (event.code === 'Space') {
        return false;
    }
};
function deleteError() {
    if (labelWithError.lastElementChild.tagName === 'DIV'){
        labelWithError.removeChild(labelWithError.lastElementChild)
    }
}
