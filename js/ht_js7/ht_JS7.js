function recursionAgain(array) {
    let ul = document.createElement('ul');
    document.body.append(ul);
    let ar = doprecursion(array);
    ar.forEach(elem =>{
        ul.insertAdjacentHTML('beforeend', elem.replace(/,/g, ''));
    });
    let div = document.createElement('div');
    document.body.append(div);
    let i = 10;
    setInterval(() => {
        div.innerText = i--;
        if (i < 0 ){
            clear()
        }},1000);
    };

function clear() {
    document.body.innerHTML = '';
}
function doprecursion(array){
 let newArray  = array.map(elem => {
     if (typeof elem === 'object' ){
         if (!Array.isArray(elem)){
             elem = Object.values(elem)
         }
         return `<li><ul>${doprecursion(elem)}</ul></li>`
     }
     return `<li>${elem}</li>`
 });
 console.log(newArray)
  return newArray
}
recursionAgain(['Ss', 'qw', 'ww', {1:'uu', 2: 'wqe', 3:['zx', 'sd',{1: 1, 2: 2}]},['uu', 'dd', ['kk', 'cc']], 'tt']);