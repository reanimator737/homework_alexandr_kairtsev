for (let key of document.querySelector('.btn-wrapper').children){
    if (key.innerText === 'Enter'){
        key.setAttribute('data-letter', `${key.innerText}`)
    } else{
        key.setAttribute('data-letter', `Key${key.innerText}`)
    }

}
let clean = document.createElement('div');
document.onkeypress = function (event) {
    for (let smth of document.querySelector('.btn-wrapper').children){
        if (smth.dataset.letter.toLowerCase() === event.code.toLowerCase()){
            clean.classList.remove('btnOnclick');
            smth.classList.add('btnOnclick');
            clean = smth;
            return
        }
    }
};