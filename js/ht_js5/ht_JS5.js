function createNewUser(firstName, lastName, data){
    let newUser={};
    Object.defineProperties(newUser, {
        'firstName': {
            get: function() { return firstName; },
            set: function(newValue) { firstName = newValue; },
        },
        'birthday': {
            get: function() { return data; },
            set: function(newValue) { data = newValue; },
        },
        'lastName': {
        get: function() { return lastName; },
        set: function(newValue) { lastName = newValue; },
    }});
    newUser.getLogin = function () {
        let login = firstName[0].toLowerCase() + lastName.toLowerCase();
        return login;
    };
    newUser.getAge = function () {
        let age = `${data.slice(-4)}.${data.slice(3,5)}.${data.slice(0,2)}`
        age = Date.parse(age);
        let ageResult = new Date(Date.now() - age);
        age = new Date(age)
        ageResult = ageResult.getFullYear() - 1970;
        return ageResult
    };
    newUser.getPassword = function () {
        let password = `${newUser.getLogin()}${data.slice(-4)}`
        return password;
    }
    return newUser;
}
let newUser = createNewUser(prompt('Enter your name'), prompt('Enter your last name'), prompt('Enter your birthday'));
console.log(`Your age = ${newUser.getAge()}`);
console.log(newUser.getPassword())
