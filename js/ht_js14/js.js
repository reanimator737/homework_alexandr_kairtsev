$(document).ready(function(){
    $("#menu").on("click","a", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
        top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
    const btnToTop = $('.scroll-top');
    const checkScrollingViewPort = (event) => {
        const scrolling = window.scrollY;
        const windowHeight = window.innerHeight;
        if (scrolling > windowHeight) {
            btnToTop.fadeIn(100);
        } else {
            btnToTop.fadeOut(100);
        }
    };
    checkScrollingViewPort();
    $(window).scroll(checkScrollingViewPort);
    btnToTop.on('click', () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    });
    const headGrid = $('.headGrid');
    const button = $('.vision');
    button.on('click', () => {
        headGrid.slideToggle()
    })
});
