function loader(flag) {
    let blur = document.querySelector('.blur');
    if (flag){
        blur.style.top =  `${getBodyScrollTop()}px`;
        blur.style.display = 'flex';
    } else {
        blur.style.display = 'none';
    }
}
function getBodyScrollTop() {
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}
export {loader, getBodyScrollTop}