import {loader, getBodyScrollTop} from './loader.js'
import {workWithForm, saveNewUser} from './new-User.js'
import {renderPost, addEventForRework, addEventForRemove} from './render.js'
start();
async function start(){
    loader(true);
    let userData =  await fetch('https://jsonplaceholder.typicode.com/users'),
        postsData = await  fetch('https://jsonplaceholder.typicode.com/posts'),
        user = await  userData.json(),
        posts = await postsData.json();
    renderPost(posts, user);
}
document.querySelector('.new-user').addEventListener('submit', event => {
    event.preventDefault();
    const formData = workWithForm();
    saveNewUser(formData);
});
document.querySelector('.add').addEventListener('click', event => {
    document.querySelector('.new-user').classList.add('active')
});