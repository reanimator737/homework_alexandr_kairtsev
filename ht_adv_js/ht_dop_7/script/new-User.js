function workWithForm() {
    const formEl = document.querySelector('.new-user');
    let data = new FormData(formEl);
    return {
        title: data.get('header') || 'header',
        body: data.get('text-content') || 'body',
        userId: 1,
    }
}
async function saveNewUser(newUser) {
    loader(true);
    let userData =  await fetch('https://jsonplaceholder.typicode.com/users'),
        user = await  userData.json(),
        postData  = await fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify(newUser),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }),
        post = await postData.json();
    renderPost([post], user, true);
    const form = document.querySelector('.new-user');
    form.reset();
    form.classList.remove('active');
}
export {workWithForm, saveNewUser}