start();
async function start(){
    loader(true);
    let userData =  await fetch('https://jsonplaceholder.typicode.com/users'),
        postsData = await  fetch('https://jsonplaceholder.typicode.com/posts'),
        user = await  userData.json(),
        posts = await postsData.json();
    renderPost(posts, user);
}
document.querySelector('.new-user').addEventListener('submit', event => {
    event.preventDefault();
    const formData = workWithForm();
    saveNewUser(formData);
});
document.querySelector('.add').addEventListener('click', event => {
    document.querySelector('.new-user').classList.add('active')
});

function renderPost(posts, users, flag) {
    posts.forEach(obj =>{
        let id  = obj.userId,
            user = users[id-1],
            div = document.createElement('div');
        div.classList.add('post-card');
        div.insertAdjacentHTML('beforeend',`
        <div class="header">
            <span class="name">${user.name}</span>
            <span class="nick">@${user.username}</span>
            <button class="btn remove">X</button>
            <button class="btn rework">R</button>
        </div>
       <h2 class="text-header">${obj.title}</h2>
       <p class="text-content">${obj.body}</p>
       <p class="footer">${user.email}</p>`);
        if (flag){
            document.querySelector('.container').prepend(div);
        } else {
            document.querySelector('.container').append(div);
        }
        addEventForRework(div, id, obj.id);
        addEventForRemove(div, id);
    });
    loader(false);
}
function addEventForRework(div, userId, id) {
    let divChildren = div.children;
    //div => div.header => button.rework
    let reworkBtn = divChildren[0].children[3];
    reworkBtn.onclick = async () => {
        loader(true);
        if (id > 100){
            loader(false);
            return (console.log('Этот сработало бы, если бы этот пост добавился бы на сервер. Important: the resource will not be really created on the server but it will be faked as if. In other words, if you try to access a post using 101 as an id, you\'ll get a 404 error'))
        }
        const newData = {
            id: id,
            title: prompt('Enter new header', 'header') || 'nice try',
            body: prompt('Enter new massage',  'massage') || 'nice try',
            userId: userId,
        };
        let data = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
                method: 'PUT',
                body: JSON.stringify(newData),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }),
            result = await data.json();
        let title =  divChildren[1],
            body = divChildren[2];
        title.innerHTML = result.title;
        body.innerHTML = result.body;
        loader(false);
    }
}
function addEventForRemove(div, id) {
    let divChildren = div.children;
    //div => div.header => button.remove
    let reworkBtn = divChildren[0].children[2];
    reworkBtn.onclick = async ()=>{
        let check = await confirm('Are you sure you want to delete these post?');
        if (check){
            await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{method: 'DELETE'});
            div.remove()
        }
    };

}
function workWithForm() {
    const formEl = document.querySelector('.new-user');
    let data = new FormData(formEl);
    return {
        title: data.get('header') || 'header',
        body: data.get('text-content') || 'body',
        userId: 1,
    }
}
async function saveNewUser(newUser) {
    loader(true);
    let userData =  await fetch('https://jsonplaceholder.typicode.com/users'),
        user = await  userData.json(),
        postData  = await fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify(newUser),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }),
        post = await postData.json();
    renderPost([post], user, true);
    const form = document.querySelector('.new-user');
    form.reset();
    form.classList.remove('active');
}
function loader(flag) {
    let blur = document.querySelector('.blur');
    if (flag){
        blur.style.top =  `${getBodyScrollTop()}px`;
        blur.style.display = 'flex';
    } else {
        blur.style.display = 'none';
    }
}
function getBodyScrollTop() {
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
}