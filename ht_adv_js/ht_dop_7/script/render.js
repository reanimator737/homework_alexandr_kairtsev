function renderPost(posts, users, flag) {
    posts.forEach(obj =>{
        let id  = obj.userId,
            user = users[id-1],
            div = document.createElement('div');
        div.classList.add('post-card');
        div.insertAdjacentHTML('beforeend',`
        <div class="header">
            <span class="name">${user.name}</span>
            <span class="nick">@${user.username}</span>
            <button class="btn remove">X</button>
            <button class="btn rework">R</button>
        </div>
       <h2 class="text-header">${obj.title}</h2>
       <p class="text-content">${obj.body}</p>
       <p class="footer">${user.email}</p>`);
        if (flag){
            document.querySelector('.container').prepend(div);
        } else {
            document.querySelector('.container').append(div);
        }
        addEventForRework(div, id, obj.id);
        addEventForRemove(div, id);
    });
    loader(false);
}
function addEventForRework(div, userId, id) {
    let divChildren = div.children;
    //div => div.header => button.rework
    let reworkBtn = divChildren[0].children[3];
    reworkBtn.onclick = async () => {
        loader(true);
        if (id > 100){
            loader(false);
            return (console.log('Этот сработало бы, если бы этот пост добавился бы на сервер. Important: the resource will not be really created on the server but it will be faked as if. In other words, if you try to access a post using 101 as an id, you\'ll get a 404 error'))
        }
        const newData = {
            id: id,
            title: prompt('Enter new header', 'header') || 'nice try',
            body: prompt('Enter new massage',  'massage') || 'nice try',
            userId: userId,
        };
        let data = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
                method: 'PUT',
                body: JSON.stringify(newData),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }),
            result = await data.json();
        let title =  divChildren[1],
            body = divChildren[2];
        title.innerHTML = result.title;
        body.innerHTML = result.body;
        loader(false);
    }
}
function addEventForRemove(div, id) {
    let divChildren = div.children;
    //div => div.header => button.remove
    let reworkBtn = divChildren[0].children[2];
    reworkBtn.onclick = async ()=>{
        await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{method: 'DELETE'});
        let check = await confirm('Are you sure you want to delete these post?');
        if (check){
            div.remove()
        }
    };

}
export {renderPost, addEventForRework, addEventForRemove}