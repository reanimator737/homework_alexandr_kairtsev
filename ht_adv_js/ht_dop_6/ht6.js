//Eсли убрать на 128 строке *3 игра поменяет идею и активной будет всегда только 1 ячейка (правда есть маленький шанс на рассинхрон, который не превышает 0.2 секунд. За 150 прокруток поймал такой 1 раз Остальные были не больше 0.01секунды что фактически не заметно)
class Game{
    constructor() {
        this.start = document.querySelector('.start');
        this.loadMenu = document.querySelector('.loadMenu');
        this.giveEventClickStart();
        this.giveEventClickLoadMenu();
    }
    giveEventClickLoadMenu() {
        this.loadMenu.onclick = function (event) {
            if (event.target.tagName === 'BUTTON' && event.target.classList.contains('choice')) {
                document.querySelector('.active-btn').classList.remove('active-btn');
                event.target.classList.add('active-btn');
            }
        }
    }
    giveEventClickStart(){
        this.start.onclick =  () => {
            this.loadMenu.style.display = 'none';
            document.querySelector('.table').style.display = 'grid';
            board = new Table(document.querySelector('.table'));
            const dataAtt = +document.querySelector('.active-btn').dataset.difficult;
            console.log(dataAtt);
            board.startGame(dataAtt);
        };
    }

}
class Table {
    #boardArray = this.generateArray();
    #userScore = 0;
    #pcScore = 0;
    constructor(selector) {
        this.board = selector;
    }
    getScore(){
        return [this.#pcScore, this.#userScore]
    }
    setScoreUser(){
        this.#userScore += 1;
    }
    setScorePC(){
        this.#pcScore += 1;
    }
    startGame(time){
        this.generateBoard();
        this.setTimeInterval(time);
    }
    generateBoard(){
        let fragment = document.createDocumentFragment();
        for(let i = 0; i < 100; i++){
            let cell = document.createElement('div');
            cell.classList.add('cell');
            fragment.append(cell)
        }
        this.board.append(fragment);
    }
    generateArray(){
        let boardArray = [];
        for(let i = 0; i < 100; i++){
            boardArray.push(i);
        }
        this.shuffleArray(boardArray);
        return boardArray
    }
    shuffleArray(array){
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }
    setTimeInterval(time){
       let intervalID = setInterval(() =>{
             if(this.#pcScore >= 50 || this.#userScore >= 50){
                 clearTimeout(intervalID);
                 this.finish();
             }
             this.setActiveZone(time);
         }, time)
        }
    setActiveZone(time){
       let numberOfCell = this.#boardArray.pop();
       let cellSelector = document.querySelectorAll('.cell')[numberOfCell];
       return (new Cell (cellSelector, time))
    }
    finish() {
        const button = document.querySelector('.restart');
        const resultTable = document.querySelector('.result-table');
        const howWin = document.querySelector('.how-win');
        const scoresPC = document.querySelector('.scores-pc');
        const scoresUser = document.querySelector('.scores-user');
        resultTable.style.display = 'flex';
        if (this.#userScore > this.#pcScore) {
            howWin.innerHTML = `Победил игрок`
        } else {
            howWin.innerHTML = `Победил компьютер`
        }
        scoresPC.innerHTML = `Очки компьютера: ${this.#pcScore}`;
        scoresUser.innerHTML = `Очки игрока: ${this.#userScore}`;
        button.onclick =  () => {
            resultTable.style.display = 'none';
            this.board.innerHTML = '';
            this.board.style.display = 'none';
            document.querySelector('.loadMenu').style.display = 'flex';
        }
    }
}
class Cell extends Table{
    constructor(event, time) {
        super(event);
        this.timer = 0;
        this.startGame(time);
    }
    startGame(time) {
        this.board.classList.add('active');
        this.timeBeforeDie(time);
        this.addEvent();
    }
    timeBeforeDie(time){
        this.timer =  setTimeout(() =>{
            if(board.getScore()[0] >= 50 || board.getScore()[1] >= 50){
                return
            }
            this.board.classList.remove('active');
            this.board.classList.add('one-point-to-PC');
            board.setScorePC();
        }, time*3);
    }
    addEvent(){
        this.board.onclick =  event =>  {
            if (event.target.classList.contains('active')){
                clearInterval(this.timer);
                event.target.classList.remove('active');
                event.target.classList.add('one-point-tp-User');
                board.setScoreUser();
            }
        }
    }

}
let board;
new Game();