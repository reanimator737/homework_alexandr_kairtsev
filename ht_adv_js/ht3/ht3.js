class Employee {
    constructor(option) {
        this.name = option.name;
        this.age =  option.age;
        this.salary = option.salary;
    }
    get fullInfo(){
        return `Имя: ${this.name}, возраст: ${this.age}, зарплата: ${this.salary}`
    }
    get userName(){
        return this.name
    }
    set userName(newValue){
        this.name =  newValue;
    }
    get userAge(){
        return this.age
    }
    set userAge(newValue){
        this.age =  newValue;
    }
    get userSalary(){
        return this.salary
    }
    set userSalary(newValue){
        this.salary =  newValue;
    }
}

class Programmer extends Employee{
    constructor(option, lang) {
        super(option);
        this.lang = lang
    }
    get fullInfo(){
        return `Имя: ${this.name}, возраст: ${this.age}, знает: ${this.lang}. Зарплата: ${this.salary*3}`
    }
    get userSalary(){
        return this.salary*3
    }
    set userSalary(newValue){
        this.salary = newValue;
    }
    get listOfLang(){
        return `${this.name} знает: ${this.lang}`
    }
}
const some1K = new Programmer({name:'K', age: 19, salary : 3000}, ['js','php']);
const userIvan = new Programmer({name: 'Ivan', age: 32, salary: 12000}, ['python', 'django ', 'C#']);
