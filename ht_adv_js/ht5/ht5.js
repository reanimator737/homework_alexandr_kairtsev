let btn = document.querySelector('.btn');
let body = document.querySelector('body');
btn.onclick =  async function (event) {
        const IP = (await (await fetch('https://api.ipify.org/?format=json')).json()).ip;
        const responseInfo = (await fetch(`http://ip-api.com/json/${IP}?lang=ru`));
        const info = await responseInfo.json();
        body.insertAdjacentHTML('beforeend',`<p>Страна - ${info.country}, город - ${info.city}. Ваш IP -${info.query}</p>`)
};