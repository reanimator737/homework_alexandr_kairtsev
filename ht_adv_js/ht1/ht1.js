const arr = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
const [city1, city2, city3, city4, city5] = arr;
console.log(city1, city2, city3, city4, city5);
console.log(...arr);
//
const Employee = {
    name: 'n a m e',
    salary: 's a l a r y'
};
const {name: emplName, salary: emplSalary} = Employee;
console.log(`Name = ${emplName}, Salary = ${emplSalary}`);
//
const array = ['value', 'showValue'],
    [value, showValue] = array;
alert(value);
alert(showValue);