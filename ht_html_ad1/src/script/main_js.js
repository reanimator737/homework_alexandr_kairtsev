$(document).ready(() =>{
   let button = $('.header_button'),
       buttonEsc = $('.button_esc'),
       buttonOption = $('.button_option'),
       navMenu = $('.header_list');
   button.click(()=>{
       if (buttonEsc.css('display') === 'none'){
           buttonOption.css('display','none');
           buttonEsc.css('display', 'block');
           navMenu.css('display', 'block');
       } else {
           buttonOption.css('display','block');
           buttonEsc.css('display', 'none');
           navMenu.css('display', 'none');
       }
   });
    $(window).resize(()=>{
        let width = $(window).width();
       if (width > 550){
           if(navMenu.css('display') !== 'flex'){
               buttonEsc.css('display', 'none');
               buttonOption.css('display','block');
               navMenu.css('display', 'flex');
           }
       }else {
           if (navMenu.css('display') === 'flex'){
               navMenu.css('display', 'none');
           }
       }
    })
});