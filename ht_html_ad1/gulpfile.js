'use strict';
let gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css'),
    browserSync = require('browser-sync').create(),
    gp = require('gulp-load-plugins')();
gulp.task('server', () => {
    browserSync.init({
        server:{
            baseDir: "./dist",
        },
    });
    browserSync.watch('build', browserSync.reload)
});
gulp.task('clean', () =>{
    return gulp.src('./dist/', {allowEmpty: true})
        .pipe(gp.clean())
});
gulp.task('buildHTML', () => {
    return gulp.src('./src/*.html')
        .pipe(gp.concat('index.html'))
        .pipe(gulp.dest('dist/'))
        .on('end', browserSync.reload);
});
gulp.task('buildCSS', () => {
    return gulp.src('./src/style/*.scss')
        .pipe(gp.sass().on('error', gp.sass.logError))
        .pipe(gp.autoprefixer({cascade: false}))
        .pipe(cleanCSS())
        .pipe(gp.concat('main.css'))
        .pipe(gulp.dest('dist/style/'))
});
gulp.task('addIMG',() =>{
    return gulp.src('./src/img/*')
        .pipe(gulp.dest('dist/img/'))
});
gulp.task('addResetCSS', () => {
    return gulp.src('./src/style/reset.css')
        .pipe(gulp.dest('dist/style/'))
});
gulp.task('buildScript', () =>{
   return gulp.src('./src/script/*.js')
       .pipe(gp.concat('main.js'))
       .pipe(gp.minify())
       .pipe(gulp.dest('dist/script/'))
});
gulp.task('watch', () => {
    gulp.watch('src/style/*.scss', gulp.series('buildCSS'));
    gulp.watch('src/script/*.js', gulp.series('buildScript'));
    gulp.watch('src/*.html', gulp.series('buildHTML'));
    gulp.watch('src/img/*', gulp.series('addIMG'));
});
gulp.task('build', gulp.series(gulp.series('clean'), gulp.parallel('buildHTML','buildCSS','buildScript', 'addResetCSS','addIMG')));
gulp.task('dev', gulp.parallel('watch','server'));