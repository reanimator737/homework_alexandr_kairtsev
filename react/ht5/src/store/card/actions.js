const findAndCut =(array, index, storage) => {
    console.log(array);
    array.splice(index, 1);
    // Создаем копию (меняем ссылку на массив)
    const newArray = array.splice(0);
    if (newArray.length === 0){
        localStorage.removeItem(storage);
    } else {
        localStorage.setItem(storage, JSON.stringify(newArray));
    }
    return newArray
};
const  pushToState = (array, elem, storage) => {
    array.push(elem);
    localStorage.setItem(storage, JSON.stringify(array));
    return array
};
export const addFavorite = (array, elem) => ({
    type: 'ADD_FAVORITE',
    payload: pushToState(array, elem, 'favorite')
});

export const removeFavorite = (array, index) => ({
    type: 'REMOVE_FAVORITE',
    payload: findAndCut(array, index, 'favorite')
});
export const addCart = (array, elem) => ({
    type: 'ADD_CART',
    payload: pushToState(array, elem, 'cart')
});
export const removeCart = (array, index) => ({
    type: 'REMOVE_CART',
    payload: findAndCut(array, index, 'cart')
});