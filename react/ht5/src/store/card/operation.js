import { addFavorite, removeFavorite, addCart, removeCart } from './actions';
export const changeCart = (state, card) => (dispatch) => {
    if (!card){
        dispatch({
            type: 'ADD_CART',
            payload: state
        })
    } else {
        const  distributor = state.findIndex(elem => elem.VendorCode === card.VendorCode);
        if(distributor !== -1){
                dispatch (removeCart(state, distributor));
        } else {
                dispatch(addCart(state, card))
        }
    }
};
export const changeFavorite = (state, card) => (dispatch) => {
    if (!card){
        dispatch({
            type: 'ADD_FAVORITE',
            payload: state
        })
    } else {
        const  distributor = state.findIndex(elem => elem.VendorCode === card.VendorCode);
        if(distributor !== -1){
            dispatch (removeFavorite(state, distributor));
        } else {
            dispatch(addFavorite(state, card))
        }
    }
};