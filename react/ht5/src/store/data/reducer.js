const initialState = {
    data: [],
    isLoading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "DATA_LOADING":
            return { ...state, isLoading: action.payload };
        case "SAVE_DATA":
            return { ...state, data: action.payload };
        default:
            return state;
    }
};
export default reducer;