import {combineReducers} from "redux";
import loadData from './data/reducer'
import card from './card/reducer'
import window from './modal/reducer'
export const reducer = combineReducers({
    loadData,
    card,
    window
});