import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {loadDATAOperation} from './store/data/operations'
import ProductList from "./component/List/ProductList";
import {Container} from "./Center";
import {Route} from "react-router-dom";
import Main from './component/main/header'
import {changeCart, changeFavorite} from "./store/card/operation";
const App = (props) => {
    const {data, loadedData, changeCart, changeFavorite, cart, favorite} = props;
    // При первой загрузке качаю данные и добавляю в стейт значения из локал сторедж
    useEffect(() => {
        if (!data.length) {
            loadedData()
        }
        changeFavorite(JSON.parse(localStorage.getItem('favorite')) || []);
        changeCart(JSON.parse(localStorage.getItem('cart')) || [])
    }, []);
    return (
        <>
            {data.length && <Container>
                <Main/>
                    <Route exact path='/' >
                        <ProductList filter={data}/>
                    </Route>
                    <Route exact path='/cart' >
                        <ProductList filter={cart} needCart={true}/>
                    </Route>
                    <Route exact path='/favorite' >
                        <ProductList filter={favorite}/>
                    </Route>
            </Container>}
        </>

    )
};
const mapStateToProps = ({ loadData, card }) => ({
    data: loadData.data,
    isLoading: loadData.isLoading,
    favorite: card.favorite,
    cart: card.cart,
});
const mapDispatchToProps = (dispatch) => ({
    loadedData: () => dispatch(loadDATAOperation()),
    changeCart: (state,id) => dispatch(changeCart(state,id)),
    changeFavorite: (state, id) => dispatch(changeFavorite(state, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App)

