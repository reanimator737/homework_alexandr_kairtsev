import styled from "styled-components";
const ModalWindowBg = styled.div`
        position: fixed;
        z-index: 3;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        background: rgba(26, 16, 16, 0.35);
        text-align: center;
        `;
const ModalWindow = styled.div`
        background: ${(props) => props.bg};
        margin: 0;
        width: 35%;
        color: white;
        `;
const Exit = styled.span`
            cursor: pointer;
            float: right;
        `;
const Header = styled.h1`
         margin: 0;
         text-align: left;
         background: #D44637;
         padding: 10px 20px
        `;
const MainText = styled.p`
         padding: 0 55px;
         font-size: 14pt;
         line-height: 40px;
`;
export  {
    ModalWindowBg,
    ModalWindow,
    Exit,
    Header,
    MainText
}