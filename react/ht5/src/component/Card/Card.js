import React, {useState} from 'react';
import  {CardBox, Image, Parag, Star} from './CardStyle.js'
import Button from '../Button/Button.js'
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
import {openWindow, closeWindow} from "../../store/modal/action";
import {changeCart, changeFavorite} from "../../store/card/operation";
import {connect} from "react-redux";
const Card = (props) => {
    const {favorite, cart, bg, id, name, color, price, changeCart, openWindow, closeWindow, modalState, changeFavorite} = props;
    const [star, toggleStar] = useState(!!favorite.find(elem => elem.VendorCode === id));
    //Хранилище вариаций текста на модалке и кнопках
    let includeInCart = !!cart.find(elem => elem.VendorCode === id);
    let changeValue = {
        text: (includeInCart && 'Remove') || 'Add to cart',
        header: (includeInCart  && 'Remove from cart') || 'Add to cart',
        modalWindowText: (includeInCart  && 'Are you sure you want to remove this item from your cart?') ||
            'Are you sure you want to add this product to your cart',
        buttonBg: (includeInCart &&'#E74C3C') || '#5bd227',
    };
    //В локал сторедже(cart, favorite) я храню карточки целиком и для оптимизации я не ищу нужные в data а формирую и передаю испоьзуя пропсы
    const thisCard = {
        "Name" : name,
        "Price" : price,
        "URL" : bg,
        "Color" : color,
        "VendorCode": id
    };
    //Эта функция открывает модалку, в стейт записывает id єлемента на котором открылась модалка.
    const openWindowContext = () =>{
        openWindow(id)
    };
    //Закрываем модальное окно, меняем локал стор и стейт корзины.
    const funcForChangeCart = () => {
      closeWindow();
      changeCart(cart, thisCard);
    };
    //Закрываем модальное окно, меняем локал стор и стейт корзины
    const funcForChangeFavorite = () => {
        changeFavorite(favorite, thisCard);
        toggleStar(!!favorite.find(elem => elem.VendorCode === id));
    };
    return(
        <>
            {modalState === id && <Modal margin={0}
                                  closeWindow={closeWindow}
                                  func = {funcForChangeCart}
                                  header={changeValue.header}
                                  text={changeValue.modalWindowText}
                                  action={['OK', 'Cancel']}> </Modal>}
            <CardBox>
                <Image bg={`./img/${bg}`}><Star onClick={funcForChangeFavorite} active={star}> </Star></Image>
                <Parag bold>{name}</Parag>
                <Parag>Color : {color}</Parag>
                <Button position={'right'}
                        margin={'3px 7px 0 0'}
                        click={openWindowContext}
                        buttonBg={changeValue.buttonBg}
                        text={changeValue.text}/>
                <Parag dopMarg bold>Price : {price}</Parag>
            </CardBox>
        </>

    )
};
const mapStateToProps = ({ card, window }) => ({
    favorite: card.favorite,
    cart: card.cart,
    modalState: window.open,
});
const mapDispatchToProps = (dispatch) => ({
    changeCart: (state,id) => dispatch(changeCart(state,id)),
    changeFavorite: (state, id) => dispatch(changeFavorite(state, id)),
    openWindow: (id) => dispatch(openWindow(id)),
    closeWindow: () => dispatch(closeWindow())
});
export default connect(mapStateToProps, mapDispatchToProps)(Card)

