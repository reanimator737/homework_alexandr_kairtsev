import React, {useState} from 'react';
import Card from "../Card/Card";
import FormikForm from "../All for Cart/form/form";
import {connect} from "react-redux";
import CartButton from '../All for Cart/Cart button/CartButtonStyle'
import {GridContainer} from "../../Center";
const ProductList = (props) => {
    const [formIsOpen, openForm] = useState(false);
    const cartHaveSmth= !!props.cart.length;

  return (
      //Рендер отфильтрованного списка элементов.
      <>
          {props.needCart && cartHaveSmth && <CartButton onClick={() => openForm(true)}>Open Cart</CartButton>}
          {formIsOpen && cartHaveSmth && <FormikForm workWithWindow={openForm} cart={props.cart} sold={props.sold}/>}
          <GridContainer>
              {props.filter.map(elem => (
                  <Card key={elem.VendorCode}
                        bg={elem.URL} name={elem.Name}
                        id={elem.VendorCode}
                        color={elem.Color} price={elem.Price}/>
                  ))}
          </GridContainer>
      </>
  )
};
const mapStateToProps = ({ card }) => ({
    cart: card.cart,
});
const mapDispatchToProps = (dispatch) => ({
    sold: () => dispatch({type: "SOLD"})
});
export default connect(mapStateToProps, mapDispatchToProps)(ProductList)