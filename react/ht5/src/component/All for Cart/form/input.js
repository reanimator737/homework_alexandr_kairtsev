import React from 'react'
import { useField } from 'formik';
import {DefaultInput, Error} from './FormStyle'
function Input(props) {
    const {name, ...rest } = props;
    const [field, meta] = useField(name);

    return (
        <>
            <div>
                    <DefaultInput {...field} {...rest} />
            </div>
            {meta.touched && meta.error && <Error>{meta.error.split(',')[0]}</Error>}
        </>
    )
}

export default Input