import React from 'react';
import { Form, withFormik } from 'formik';
import Input from './input';
import * as yup from 'yup';
import { ModalWindowBg} from '../../Modal/ModalStyle';
import {Header, Submit} from "./FormStyle";
import {changeCart, changeFavorite} from "../../../store/card/operation";
import {closeWindow, openWindow} from "../../../store/modal/action";
import {connect} from "react-redux";
const FIELD_REQUIRED = 'This field is required';
const schema = yup.object().shape({
    name: yup
        .string()
        .required(FIELD_REQUIRED),
    lastName: yup
        .string()
        .required(FIELD_REQUIRED),
    age: yup
         .number()
        .positive()
        .required(FIELD_REQUIRED),
    address: yup
        .string()
        .required(FIELD_REQUIRED),
    phoneNumber: yup
        .string()
        .matches('^\\+?3?8?(0[5-9][0-9]\\d{7})$')
        .required(FIELD_REQUIRED)
});

const handleSubmit = (values, {props}) => {
    props.workWithWindow(false);
    console.log(values);
    console.log(props.cart);
    localStorage.setItem('cart', JSON.stringify([]));
    props.sold()
};
const FormikForm = (props) => {
    const closeWindow = (event)=> {
       if(event.currentTarget === event.target){
           props.workWithWindow(false)
       }
    };
    return (
        <ModalWindowBg onClick={closeWindow}>
            <Form noValidate>
                <Header>Registration form</Header>
                <Input type='text' placeholder='Your first name' name='name' needTopBorder={true} />
                <Input type='text' placeholder='Your second name' name='lastName' />
                <Input type='text' placeholder='Your age' name='age' />
                <Input type='text' placeholder='Your address' name='address'/>
                <Input type='text' placeholder='Your phone number' name='phoneNumber'/>
                <div>
                    <Submit type='submit'>Checkout</Submit>
                </div>
            </Form>
        </ModalWindowBg>
    )
};
const mapStateToProps = ({ card}) => ({
    cart: card.cart,
});
const mapDispatchToProps = (dispatch) => ({
    changeCart: (state,id) => dispatch(changeCart(state,id)),
    changeFavorite: (state, id) => dispatch(changeFavorite(state, id)),
    openWindow: (id) => dispatch(openWindow(id)),
    closeWindow: () => dispatch(closeWindow())
});
const AddForm =connect(mapStateToProps, mapDispatchToProps)(FormikForm);
export default withFormik({
    mapPropsToValues: (props) => ({
        name: '',
        lastName: '',
        age: '',
        address: '',
        phoneNumber: ''
    }),
    handleSubmit,
    validationSchema: schema
})(AddForm)