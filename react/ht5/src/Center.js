import styled from "styled-components";

const Container = styled.div`
        background-image: url('bg.jpg');
        background-repeat: no-repeat;
        background-size: 100%;
        min-height: 100vh;
        `;
const GridContainer = styled.div`
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        grid-gap: 25px;
        padding: 20px;
`;
export {
        Container,
        GridContainer
}