import React, {Component} from 'react';
import DefaultButton from './ButtonStyle.js'

export default class Button extends Component{
    render() {
        return(
            <DefaultButton background={this.props.background} default={this.props.default} onClick={()=> this.props.func(this.props.value)}>
                {this.props.text}
            </DefaultButton>
        )
    }
}