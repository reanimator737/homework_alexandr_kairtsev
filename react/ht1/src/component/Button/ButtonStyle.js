import React, {Component} from 'react';
import styled from "styled-components";
const DefaultButton = styled.button`
        padding: ${(props) => (props.default ? '10px 15px' : '10px 30px')};  
        border-radius: 5px;
        font-size: 14pt;
        color: white;
        text-transform: uppercase;
        width: ${(props) => (props.default ? '105px' : 'auto')};
        margin: ${(props) => (props.default ? '0px 20px 10px 20px' : 'auto')};
        border: 0;
        cursor: pointer;
        background: ${(props) => (props.background)};
        `;
export default DefaultButton;