import React, {Component} from 'react';
import {ModalWindowBg, ModalWindow, Exit, Header, MainText} from './ModalStyle.js'
import  DefaultButton from '../Button/ButtonStyle.js'

export default class Modal extends Component {
    state ={
        closeButton: this.props.closeButton
    };
    closeWindow(event){
        if (event.target === event.currentTarget){
            this.props.func(this.props.value);
        }
    }
    render() {
        console.log(this.props);
        const exit = this.props.closeButton && <Exit onClick={(event) => this.closeWindow(event)}>X</Exit>
        return (
            <ModalWindowBg onClick={(event) => this.closeWindow(event)}>
                <ModalWindow background={this.props.background}>
                    <Header>
                        {this.props.header}
                        {exit}
                    </Header>
                    <MainText>{this.props.text}</MainText>
                    {this.props.action[0]}
                    {this.props.action[1]}
                </ModalWindow>
            </ModalWindowBg>
        )
    }
}