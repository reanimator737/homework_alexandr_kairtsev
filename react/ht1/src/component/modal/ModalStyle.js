import styled from "styled-components";
import React, {Component} from 'react';
const ModalWindowBg = styled.div`
        position: absolute;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        background: rgba(26, 16, 16, 0.35);
        text-align: center;
        `;
const ModalWindow = styled.div`
        margin: 0;
        background: ${(props) => (props.background)};
        width: 40%;
        color: white;
        `;
const Exit = styled.span`
            cursor: pointer;
            float: right;
        `;
const Header = styled.h1`
         margin: 0;
         text-align: left;
         background: #D44637;
         padding: 10px 20px
        `;
const MainText = styled.p`
         padding: 0 55px;
         font-size: 14pt;
         line-height: 40px;
`;
export  {
    ModalWindowBg,
    ModalWindow,
    Exit,
    Header,
    MainText
}