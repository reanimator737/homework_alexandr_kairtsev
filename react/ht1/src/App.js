import React, {Component} from 'react';
import './App.css';
import Modal from './component/modal/Modal'
import Button from './component/Button/Button.js'
import styled from "styled-components";
const Center = styled.div`
            display: flex;
            justify-content: space-around;
            `;
class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            first: false,
            second: false
        };
    }
    openModal(value){
        this.setState({
            [value]: !this.state[value]
        })
    }
    render() {
        const func = this.openModal.bind(this);
        const firstWindowButton = [<Button background={'#B3382C'}
                                           text={'ok'}
                                           func={func}
                                           value={'first'}
                                           default={'default'}/>,
                                    <Button background={'#B3382C'}
                                            text={'close'} func={func}
                                            value={'first'}
                                            default={'default'}/>];
        const secondWindowButton = [<Button background={'black'}
                                            text={'help'}
                                            func={func}
                                            value={'second'}
                                            default={'default'}/>,
                                    <Button background={'black'}
                                            text={'me'}
                                            func={func}
                                            value={'second'}
                                            default={'default'}/>];
        const textFirstModal = 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?';
        const textSecondModal = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        const firstWindow = this.state.first && <Modal background={'#E74C3C'}
                                                       header={'Do you want to delete this file?'}
                                                       action={firstWindowButton}
                                                       func={func}
                                                       value={'first'}
                                                       closeButton={true}
                                                       text={textFirstModal}> </Modal>;
        const secondWindow = this.state.second && <Modal background={'#ba721a'}
                                                         header={'Completely different text'}
                                                         action={secondWindowButton}
                                                         func={func}
                                                         value={'second'}
                                                         closeButton={true}
                                                         text={textSecondModal}> </Modal>;
        const modal = firstWindow || secondWindow;
        return (
           <Center>
               {modal}
               <Button background={'green'} text={'Open first modal'} func={func} value={'first'}>
               </Button>
               <Button background={'violet'} text={'Open second modal'} func={func} value={'second'}>
               </Button>
           </Center>
        )
    }
}
export default App;
