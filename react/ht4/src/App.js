import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {loadDATAOperation} from './reducers/data/operations'
import ProductList from "./component/List/ProductList";
import {Container, GridContainer} from "./Center";
import {Route} from "react-router-dom";
import Main from './component/main/header'
import {changeCart, changeFavorite} from "./reducers/card/operation";
const App = (props) => {
    const {data, loadedData, changeCart, changeFavorite, cart, favorite} = props;
    // При первой загрузке качаю данные и добавляю в стейт значения из локал сторедж
    useEffect(() => {
        if (!data.length) {
            loadedData()
        }
        changeFavorite(JSON.parse(localStorage.getItem('favorite')) || []);
        changeCart(JSON.parse(localStorage.getItem('cart')) || [])
    }, []);
    //Фильтер списка элементов на экране
    const  filterContent = (target) => {
     return  data.filter(elem => target.includes(elem.VendorCode))
    };
    return (
        <>
            {data.length && <Container>
                <Main/>
                <GridContainer>
                    <Route exact path='/' render={() => <ProductList filter={data} />} />
                    <Route exact path='/cart' render={() => <ProductList filter={filterContent(cart)}/>} />
                    <Route exact path='/favorite' render={() => <ProductList filter={filterContent(favorite)}/>} />
                </GridContainer>
            </Container>}
        </>

    )
};
const mapStateToProps = ({ loadData, card }) => ({
    data: loadData.data,
    isLoading: loadData.isLoading,
    favorite: card.favorite,
    cart: card.cart,
});
const mapDispatchToProps = (dispatch) => ({
    loadedData: () => dispatch(loadDATAOperation()),
    changeCart: (state,id) => dispatch(changeCart(state,id)),
    changeFavorite: (state, id) => dispatch(changeFavorite(state, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App)

