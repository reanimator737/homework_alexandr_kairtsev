        import styled from "styled-components";
const DefaultButton = styled.button`
        padding: ${(props) => (props.default ? '10px 15px' : '5px 10px')};  
        border-radius: 5px;
        font-size: 12pt;
        color: white;
        text-transform: uppercase;
        width: ${(props) => (props.default ? '105px' : 'auto')};
        margin: ${(props) => (props.default ? '0px 20px 10px 20px' : 'auto')};
        border: 0;
        cursor: pointer;
        background: ${(props) => (props.buttonBg)};
        float: ${(props) => (props.position)};
        margin: ${(props) => (props.margin)};
        
        `;
export default DefaultButton;