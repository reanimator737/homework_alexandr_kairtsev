import React from 'react';
import DefaultButton from './ButtonStyle.js'
const  Button = (props) => {
    return(
        <>
            <DefaultButton buttonBg={props.buttonBg}
                           position={props.position}
                           margin={props.margin}
                           default={props.default}
                           onClick={(event) => {
                               props.click()
                           }}>
                {props.text}
            </DefaultButton>
        </>
    )
};
export default Button
