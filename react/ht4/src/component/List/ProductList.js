import React from 'react';
import Card from "../Card/Card";
import {connect} from "react-redux";

const ProductList = (props) => {
  return (
      //Рендер отфильтрованного списка элементов.
      props.filter.map(elem => (
          <Card key={elem.VendorCode}
                    bg={elem.URL} name={elem.Name}
                    id={elem.VendorCode}
                    color={elem.Color} price={elem.Price}/>
          ))

  )
};
const mapStateToProps = ({card }) => ({
    favorite: card.favorite,
    cart: card.cart,
});
export default connect(mapStateToProps)(ProductList)