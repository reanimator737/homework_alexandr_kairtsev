import axios from 'axios';
import { dataLoading, saveData } from './actions';

export const loadDATAOperation = () => (dispatch) => {
    dispatch(dataLoading(true));
    axios('./data.json')
        .then(res => {
            dispatch(saveData(res.data));
            dispatch(dataLoading(false));
        })
};