const initialState = {
    favorite: [],
    cart: []
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_FAVORITE":
            return { ...state, favorite: action.payload };
        case "REMOVE_FAVORITE":
            return { ...state, favorite: action.payload };
        case "ADD_CART":
            return { ...state, cart: action.payload };
        case "REMOVE_CART":
            return { ...state, cart: action.payload };
        default:
            return state;
    }
};
export default reducer;