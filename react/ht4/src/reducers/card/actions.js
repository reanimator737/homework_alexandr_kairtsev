const findAndCut =(array, elem) =>{
    array.splice(array.indexOf(elem),1);
    return array
};
const  pushToState = (array, elem) => {
    array.push(elem);
    return array
};
export const addFavorite = (array, elem) => ({
    type: 'ADD_FAVORITE',
    payload: pushToState(array, elem)
});

export const removeFavorite = (array, elem) => ({
    type: 'REMOVE_FAVORITE',
    payload: findAndCut(array, elem)
});
export const addCart = (array, elem) => ({
    type: 'ADD_CART',
    payload: pushToState(array, elem)
});
export const removeCart = (array, elem) => ({
    type: 'REMOVE_CART',
    payload: findAndCut(array, elem)
});