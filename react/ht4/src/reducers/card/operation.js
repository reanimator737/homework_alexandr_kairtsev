import { addFavorite, removeFavorite, addCart, removeCart } from './actions';

export const changeCart = (state, id) => (dispatch) => {
    if (!id){
        dispatch({
            type: 'ADD_CART',
            payload: state
        })
    }else if (!state.includes(id)){
        dispatch(addCart(state, id))
    } else {
        dispatch (removeCart(state, id));
    }
};
export const changeFavorite = (state, id) => (dispatch) => {
    if (!id){
        dispatch({
            type: 'ADD_FAVORITE',
            payload: state
        })
    }else if (!state.includes(id)){
        dispatch(addFavorite(state, id))
    } else {
        dispatch (removeFavorite(state, id));
    }
};