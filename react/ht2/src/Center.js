import styled from "styled-components";

const Container = styled.div`
        background-image: url('bg.jpg');
        background-repeat: no-repeat;
        background-size: 100%;
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        grid-gap: 25px;
        padding: 10px;
        `;
export default  Container