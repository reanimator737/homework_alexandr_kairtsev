import React, {Component} from 'react';
import axios from 'axios'
import Card from "./component/Card/Card";
import Container from "./Center";
class App extends Component {
    constructor(props) {
        super(props);
        this.state={
            items: []
        };
    }
    componentDidMount() {
        axios("/data.json")
            .then(res => this.setState({items: res.data}))
    }
    render() {
        return (
            <Container>
                {this.state.items.map(elem => (
                    <Card key={elem.VendorCode}
                          bg={elem.URL} name={elem.Name}
                          id={elem.VendorCode}
                          color={elem.Color} price={elem.Price}/>
                ))}
            </Container>
        )
    }
}
export default App;
