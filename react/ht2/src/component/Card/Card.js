import React, {Component} from 'react';
import  {CardBox, Image, Parag, Star} from './CardStyle.js'
import Button from '../Button/Button.js'
import PropTypes from 'prop-types';

export default class Card extends Component{
    state = {
      star: false
    };
    componentDidMount() {
        const infoInStorage = localStorage.getItem(`${this.props.id}-star`) === 'true';
        console.log(infoInStorage);
        this.setState({ star: infoInStorage });
    }
    starClick(){
        if(this.state.star === false){
            localStorage.setItem(`${this.props.id}-star`, 'true');
            this.setState({
                star: true
            })
        } else {
            localStorage.removeItem(`${this.props.id}-star`);
            this.setState({
                star: false
            })
        }
    }
    render() {
            return(
                <CardBox>
                    <Image bg={this.props.bg}><Star onClick={this.starClick.bind(this)} active={this.state.star}> </Star></Image>
                    <Parag bold>{this.props.name}</Parag>
                    <Parag>Color : {this.props.color}</Parag>
                    <Button buttonBg={this.props.buttonBg}
                            position={'right'}
                            margin={'3px 7px 0 0'}
                            text={['Add to cart', 'Remove']}
                            header={[ 'Add to cart', 'Remove from cart']}
                            id={this.props.id}
                            modalWindowText = {['Are you sure you want to add this product to your cart',
                                 'Are you sure you want to remove this item from your cart?']}/>
                    <Parag dopMarg bold>Price : {this.props.price}</Parag>
                </CardBox>
            )
    }
}
Card.propTypes = {
    buttonBg: PropTypes.string,
    bg: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};
