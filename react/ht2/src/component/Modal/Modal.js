import React, {Component} from 'react';
import {ModalWindowBg, ModalWindow, Header, MainText} from './ModalStyle.js'
import  DefaultButton from '../Button/ButtonStyle.js'
import PropTypes from "prop-types";
export default class Modal extends Component {
    outsideClick(event){
        if (event.target === event.currentTarget){
            this.props.func()
        }
    }
    render() {
        return (
            <ModalWindowBg onClick={(event) => this.outsideClick(event)}>
                <ModalWindow bg={'#E74C3C'}>
                    <Header>
                        {this.props.header}
                    </Header>
                    <MainText>{this.props.text}</MainText>
                    <DefaultButton  buttonBg={this.props.buttonBg} default onClick={() => this.props.func(true)}>{this.props.action[0]}</DefaultButton>
                    <DefaultButton  buttonBg={this.props.buttonBg} default onClick={() => this.props.func()}>{this.props.action[1]}</DefaultButton>
                </ModalWindow>
            </ModalWindowBg>
        )
    }
}
Modal.propsTypes = {
    func: PropTypes.func,
    header: PropTypes.string,
    action: PropTypes.array
};
Modal.defaultProps = {
    header: ['Modal window', 'Modal window'],
    action: ['OK', 'Сancel'],
    buttonBg: '#B3382C'
};