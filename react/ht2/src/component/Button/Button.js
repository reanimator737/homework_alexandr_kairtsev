import React, {Component} from 'react';
import DefaultButton from './ButtonStyle.js'
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

export default class Button extends Component{
    state = {
        openModal: false,
        active : true,
    };
    componentDidMount() {
        const infoInStorage = localStorage.getItem(this.props.id) === 'false';
        this.setState({ active: !infoInStorage });
    }
    clickFunc(value){
        if (value === true){
            value = !this.state.active;
            if (!!localStorage.getItem(this.props.id)){
                localStorage.removeItem(this.props.id)
            } else {
                localStorage.setItem(this.props.id, 'false')
            }
        } else {
            value = this.state.active
        }
        this.setState({
            openModal: !this.state.openModal,
            active: value
        });
    }
    render() {
        const text = (this.state.active && this.props.text[0]) || (!this.state.active && this.props.text[1]);
        const header = (this.state.active && this.props.header[0]) || (!this.state.active && this.props.header[1]);
        const modalWindowText = (this.state.active && this.props.modalWindowText[0]) || (!this.state.active && this.props.modalWindowText[1]);
        return(
            <>
                <DefaultButton buttonBg={this.props.buttonBg}
                               position={this.props.position}
                               margin={this.props.margin}
                               onClick={() => {this.clickFunc('true')}}>
                    {text}
                </DefaultButton>
                {this.state.openModal && <Modal action={this.props.action}
                                                key={this.props.key}
                                                header={header}
                                                func={this.clickFunc.bind(this)}
                                                text={modalWindowText}> </Modal>}
            </>

        )
    }
}
Button.propTypes = {
    text: PropTypes.array,
    header: PropTypes.array,
    modalWindowText: PropTypes.array,
    action: PropTypes.array,
    price: PropTypes.string,
    buttonBg: PropTypes.string,
    position: PropTypes.string,
    margin: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};
Button.defaultProps = {
    position: 'right',
    buttonBg: '#1e1e20',
};