import styled from "styled-components";

const Header = styled.ul`
        display: grid;
        grid-template-columns: repeat(3, 1fr);
`;
const HeaderComp = styled.li`
        background: black;
        padding: 20px 0;
        color: yellow;
        font-size: 16pt;
        text-align: center;
        border: 1px solid yellow;
        font-weight: 500;
        text-transform: uppercase;
        &:hover{
            color: #FF8C00;
            border: 1px solid #FF8C00;
        }
`;

export {
    Header,
    HeaderComp
}