import React from "react";
import {Header, HeaderComp} from "./headerStyle";
import {Link} from "react-router-dom";

const Main = () => {
    return (
        <Header>
            <Link to={"/"}><HeaderComp>Storage</HeaderComp></Link>
            <Link to={"/cart"}><HeaderComp>Cart</HeaderComp></Link>
            <Link to={"/favorite"}><HeaderComp>Favorite</HeaderComp></Link>
        </Header>
    )
};
export default Main