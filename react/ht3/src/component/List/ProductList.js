import React from 'react';
import Card from "../Card/Card";

const ProductList = (props) => {
    let {items, filter} = props;
    if (filter){
        items = items
            .filter(elem => JSON.parse((localStorage.getItem(filter) || []).includes(elem.VendorCode)));
    }
  return (
      items
          .map(elem => (
              <Card key={elem.VendorCode}
                    bg={elem.URL} name={elem.Name}
                    id={elem.VendorCode}
                    buttonBg={'#ffc7b0'}
                    cart={props.cart}
                    star={props.star}
                    color={elem.Color} price={elem.Price}/>
          ))

  )
};
export default ProductList;