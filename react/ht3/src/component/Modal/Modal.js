import React from 'react';
import {ModalWindowBg, ModalWindow, Header, MainText} from './ModalStyle.js'
import PropTypes from "prop-types";
import Button from "../Button/Button";
const Modal = (props) => {
    const outsideClick = (event) =>{
        if (event.target === event.currentTarget){
            props.func()
        }};
        return (
            <ModalWindowBg onClick={(event) => outsideClick(event)}>
                <ModalWindow bg={'#E74C3C'}>
                    <Header>
                        {props.header}
                    </Header>
                    <MainText>{props.text}</MainText>
                    <Button  buttonBg={props.buttonBg} click={() => props.func(true)} text={props.action[0]} default={'default'}> </Button>
                    <Button  buttonBg={props.buttonBg}  click={() => props.func()} text={props.action[1]} default={'default'}> </Button>
                </ModalWindow>
            </ModalWindowBg>
        )
};
export default Modal
Modal.propsTypes = {
    func: PropTypes.func,
    header: PropTypes.string,
    action: PropTypes.array
};
Modal.defaultProps = {
    header: ['Modal window', 'Modal window'],
    action: ['OK', 'Сancel'],
    buttonBg: '#B3382C'
};