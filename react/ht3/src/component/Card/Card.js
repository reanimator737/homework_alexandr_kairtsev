import React, {useState} from 'react';
import  {CardBox, Image, Parag, Star} from './CardStyle.js'
import Button from '../Button/Button.js'
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
const Card = (props) => {
    const starProps = props.star;
    const cartProps = props.cart;
    const [openModal, open] = useState(false);
    const starClick = () => {
        const localNow = starProps.info;
        if(!localNow.includes(props.id)){
            localNow.push(props.id);
            localStorage.setItem(`star`, JSON.stringify(localNow));
        } else {
            localNow.splice(localNow.indexOf(props.id), 1);
            localStorage.setItem(`star`, JSON.stringify(localNow));
        }
        starProps.func(JSON.parse(localStorage.getItem('star')));
    };
    const clickFuncCart = () =>{
        open(!openModal);
    };
    const modalClick = (value) => {
        if (value){
            const localNowCart = cartProps.info;
            if(!localNowCart.includes(props.id)){
                localNowCart.push(props.id);
                localStorage.setItem(`cart`, JSON.stringify(localNowCart));
            } else {
                localNowCart.splice(localNowCart.indexOf(props.id), 1);
                localStorage.setItem(`cart`, JSON.stringify(localNowCart));
            }
            cartProps.func(JSON.parse(localStorage.getItem('cart')));
        }
        open(!openModal);
    };
    const changeValue = {
        text: (cartProps.info.includes(props.id) && 'Remove') || 'Add to cart',
        header: (cartProps.info.includes(props.id)  && 'Remove from cart') || 'Add to cart',
        modalWindowText: (cartProps.info.includes(props.id)  && 'Are you sure you want to remove this item from your cart?') ||
            'Are you sure you want to add this product to your cart'
    };
    return(
        <>
            {openModal && <Modal margin={0}
                                 func={modalClick}
                                 header={changeValue.header}
                                 text={changeValue.modalWindowText}
                                 action={['OK', 'Cancel']}
                                 background={props.buttonBg}> </Modal>}
            <CardBox>
                <Image bg={`./img/${props.bg}`}><Star onClick={starClick} active={starProps.info.includes(props.id)}> </Star></Image>
                <Parag bold>{props.name}</Parag>
                <Parag>Color : {props.color}</Parag>
                <Button buttonBg={props.buttonBg}
                        position={'right'}
                        margin={'3px 7px 0 0'}
                        text={changeValue.text}
                        click={clickFuncCart}/>
                <Parag dopMarg bold>Price : {props.price}</Parag>
            </CardBox>
            </>
    )
};
export default Card
Card.propTypes = {
    buttonBg: PropTypes.string,
    bg: PropTypes.string,
    name: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    id: PropTypes.number || PropTypes.string
};
