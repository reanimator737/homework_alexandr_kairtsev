import React, {useState, useEffect} from 'react';
import Main from './component/main/header'
import axios from 'axios'
import {Route} from "react-router-dom";
import ProductList from "./component/List/ProductList";
import {Container, GridContainer} from "./Center";
const App = () => {
    const [items, addItems] = useState([]);
    const [cart, reworkCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [star, reworkStar] = useState(JSON.parse(localStorage.getItem('star')) || []);
    let cartInfo = {
        info: cart,
        func: reworkCart
    };
    let starInfo = {
        info: star,
        func: reworkStar
    };
    useEffect(() =>{
        if(items.length === 0){
            axios("/data.json")
                .then(res => addItems(res.data))
        }
    },[cartInfo, starInfo]);
    return (
        <>
            <Container>
                <Main/>
                <GridContainer>
                    <Route exact path='/' render={() => <ProductList items={items}
                                                                     cart={cartInfo}
                                                                     star={starInfo}/>} />
                    <Route exact path='/cart' render={() => <ProductList items={items}
                                                                         filter={'cart'}
                                                                         cart={cartInfo}
                                                                         star={starInfo}/>} />
                    <Route exact path='/favorite' render={() => <ProductList items={items}
                                                                             filter={'star'}
                                                                             cart={cartInfo}
                                                                             star={starInfo}/>} />
                </GridContainer>
            </Container>
        </>
    )
};
export default App;
