import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import {render, screen} from "@testing-library/react";
import {Provider} from "react-redux";
import React from "react";
import App from './App'
import {Router} from "react-router-dom";
import { createMemoryHistory } from 'history'
import {loadDATAOperation} from './store/data/operations'
jest.mock('./store/data/operations');
const testAction = {type: 'TEST_LOAD'};
loadDATAOperation.mockReturnValue(testAction);
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
function createPropsAndStore(data,favorite, cart){
    return {
        store: mockStore({
            card: {
                favorite: favorite || [],
                cart: cart || []
            },
            loadData: {
                data: data || [],
                isLoading: false
            }
        })
    }
}
describe('Testing App (Router and async get data)', () => {
    test('Smoke test', () => {
        const history = createMemoryHistory();
        const {store, props} = createPropsAndStore();
        render(<Provider store={store}><Router history={history}><App {...props}/></Router></Provider>);
    });
    test('Get data',  ()=> {
        const history = createMemoryHistory();
        const {store, props} = createPropsAndStore();
        render(<Provider store={store}><Router history={history}><App {...props}/></Router></Provider>);
        //Происходит 3 экшена, 1 загрузка данных, 2 и 3 добавление в стор данных из localStorage
        expect(store.getActions()[0]).toEqual(testAction);
    });
    test('Render cardList', ()=>{
        const history = createMemoryHistory();
        const {store} = createPropsAndStore([{VendorCode:1111}, {VendorCode: 2222}]);
        render(<Provider store={store}><Router history={history}><App/></Router></Provider>);
        const list = screen.getByTestId('list');
        //Проверка правильной работы ProductList и правильной передачи пропсов из редакса
        expect(list.children.length).toBe(2)
    });
    test('Render cardList(favorite)', ()=>{
        const history = createMemoryHistory();
        history.push('/favorite');
        const {store} = createPropsAndStore([{VendorCode:1111}, {VendorCode: 2222}], [{VendorCode: 1111}]);
        render(<Provider store={store}><Router history={history}><App/></Router></Provider>);
        const list = screen.getByTestId('list');
        expect(list.children.length).toBe(1)
    });
    test('Render cardList(cart)', ()=>{
        const history = createMemoryHistory();
        history.push('/cart');
        const {store} = createPropsAndStore([{VendorCode:1111}, {VendorCode: 2222}], [], [{VendorCode: 1111}]);
        render(<Provider store={store}><Router history={history}><App/></Router></Provider>);
        const list = screen.getByTestId('listWithCart');
        expect(list.children.length).toBe(1);
    })
});