import Modal from "./Modal";
import React from "react";
import {render, screen} from "@testing-library/react";

describe('Test modal window', ()=>{
    test('Smoke test modal window', () => {
        render(<Modal/>)
    });
    test('Test all click in modal window', ()=>{
        //Я считаю доцельно в 1 тесте прокликать все, что бы быть увереным что клик на одном элемент не вызывает ивент на другом элементе
        const OutsideClick = jest.fn();
        const Done = jest.fn();
        //OutsideClick - вызываем при клике не по модалке и на кнопку Close, Done - вызываем при клике на кнопку Accept
        const action = ['Accept', 'Close'];
        render(<Modal text={'smth'} closeWindow={OutsideClick} func={Done} action={action}/>);
        const bg = screen.getByTestId('bg');
        const modal = screen.getByTestId('modal');
        const accept = screen.getByText('Accept');
        const close = screen.getByText('Close');
        expect(OutsideClick).toHaveBeenCalledTimes(0);
        expect(Done).toHaveBeenCalledTimes(0);
        modal.click();
        expect(OutsideClick).toHaveBeenCalledTimes(0);
        expect(Done).toHaveBeenCalledTimes(0);
        bg.click();
        expect(OutsideClick).toHaveBeenCalledTimes(1);
        expect(Done).toHaveBeenCalledTimes(0);
        close.click();
        expect(OutsideClick).toHaveBeenCalledTimes(2);
        expect(Done).toHaveBeenCalledTimes(0);
        accept.click();
        expect(OutsideClick).toHaveBeenCalledTimes(2);
        expect(Done).toHaveBeenCalledTimes(1);
    })

});