import styled from "styled-components";

const CardBox = styled.div`
        padding: 0;
        font-size: 14pt;
        background: white;
        border-radius: 5px;
        `;
const Image = styled.div`
        width: 100%;
        height: 300px;
        background-image: ${(props)=> `url(${props.bg})`};
        background-repeat: no-repeat;
        background-position: center;
        background-size: 100% 90%;
`;
const Parag = styled.p`
        margin: ${(param) => (param.dopMarg? '10px 5px' : '5px')};
        font-weight: ${(param) => param.bold? '600' : 'normal'};
`;
const Star = styled.div`
        margin: 10px 5px; 
        height: 0; 
        width: 0;
        position: relative;
        border-right: 12px solid transparent;
        border-bottom: ${(props) => props.active === true ? '8px solid green' : '8px solid black'};
        border-left: 12px solid transparent;
        transform: rotate(35deg);
        ::before, ::after {
            content: "";
            height: 0; 
            width: 0;
            position: absolute;
        }
        ::before {
            top: -6px; 
            left: -7px;
            border-bottom: ${(props) => props.active === true? '10px solid green' : '10px solid black'};
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            transform: rotate(-35deg);
        }
        ::after {
            top: 0.5px; 
            left: -13px;
            border-right: 12px solid transparent;
            border-bottom: ${(props) => props.active === true? '8px solid green' : '8px solid black'};
            border-left: 12px solid transparent;
            transform: rotate(-70deg);
        }
`;
export {
    CardBox,
    Image,
    Parag,
    Star
}