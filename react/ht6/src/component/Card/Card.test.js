import React from "react";
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import {render, screen} from '@testing-library/react'
import Card from "./Card";
import {Provider} from 'react-redux'
import 'jest-styled-components'

const testValueInStore =  {
    "Name" : "Samsung Galaxy S20",
    "Price" : "640$",
    "URL" : "SamsungGalaxyS20.jpg",
    "Color" : 'Navy',
    "VendorCode": '1111'
};
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
function createPropsAndStore(favorite, cart){
    return {
        props: {
            "name" : "Samsung Galaxy S20",
            "price" : "640$",
            "bg" : "SamsungGalaxyS20.jpg",
            "color" : "Navy",
            "id": '1111'
        },
        store: mockStore({
            card: {
                favorite: favorite || [],
                cart: cart || []
            }
        })
    }
}
describe('Testing card', () => {
    test('Smoke test card with all props', () => {
        const {store, props} = createPropsAndStore();
        render(<Provider store={store}><Card {...props}/></Provider>);
        const color = screen.getByText(`Color : ${props.color}`);
        const name = screen.getByText(props.name);
        const price = screen.getByText(`Price : ${props.price}`);
        // Проверка кнопки еще и одновременно проверяет текст в модалке
        const button = screen.getByText('Add to cart');
    });
    test('Press button add to cart', () => {
        const {store, props} = createPropsAndStore();
        render(<Provider store={store}><Card {...props}/></Provider>);
        const button = screen.getByText('Add to cart');
        button.click();
        //Появление модалки означает что локальный стейт изменился а функция openWindowContext отработала правильно
        const modal = screen.getByText('Are you sure you want to add this product to your cart?');
        const buttonOK = screen.getByText('OK');
        buttonOK.click();
        //После нажатия на окей, поменялся глобальный стейт и кнопка поменяла название
        //Dispatch отправляет объект который собирается внутри карточки
        expect(button.innerHTML).toBe('Remove');
    });
    test('Press button star', () => {
        const {store, props} = createPropsAndStore();
        render(<Provider store={store}><Card {...props}/></Provider>);
        const star = screen.getByTestId('star');
        const className = star.className;
        star.click();
        const newClassName = star.className;
        //Изменение классов === изменился стейт
        expect(className).not.toBe(newClassName);
    });
    test('Id in cart', () => {
        const {store, props} = createPropsAndStore([], [testValueInStore]);
        render(<Provider store={store}><Card {...props}/></Provider>);
        //Если айди карточки лежит в сторе, значит значение текста кнопки и модалки отличается от стандартного
        const button = screen.getByText('Remove');
        button.click();
        screen.getByText('Are you sure you want to remove this item from your cart?')
    });
    test('Id in favorite', () => {
        const {store, props} = createPropsAndStore([testValueInStore], []);
        render(<Provider store={store}><Card {...props}/></Provider>);
        const star = screen.getByTestId('star');
        // Проверка цвета звездочки (должен быть зеленый)
        expect(star).toHaveStyleRule('border-bottom','8px solid green')
    });
});