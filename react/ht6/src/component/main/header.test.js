import {render, screen} from "@testing-library/react";
import React from "react";
import {Router} from "react-router-dom";
import { createMemoryHistory } from 'history'
import Header from './header'
describe('Test header (link system)', () => {
    test('Smoke test', () => {
        const history = createMemoryHistory();
        const {container} = render(<Router history={history}><Header/></Router>);
        expect(container.children[0].children.length).toBe(3)
    });
    test('Click on Storage', () => {
        const history = createMemoryHistory();
        const {container} = render(<Router history={history}><Header/></Router>);
        screen.getByText('Storage').click();
        expect(history.location.pathname).toBe('/')
    });
    test('Click on Cart', () => {
        const history = createMemoryHistory();
        const {container} = render(<Router history={history}><Header/></Router>);
        screen.getByText('Cart').click();
        expect(history.location.pathname).toBe('/cart')
    });
    test('Click on Favorite', () => {
        const history = createMemoryHistory();
        const {container} = render(<Router history={history}><Header/></Router>);
        screen.getByText('Favorite').click();
        expect(history.location.pathname).toBe('/favorite')
    });
});