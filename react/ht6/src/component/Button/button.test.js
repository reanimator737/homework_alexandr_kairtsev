import Button from "./Button";
import React from "react";
import {render} from "@testing-library/react";

describe('Test button', ()=>{
    test('Smoke test button', () => {
        render(<Button/>)
    });
    test('Click on button test', ()=>{
        const clickFuncMock = jest.fn();
        const {getByText} = render(<Button text={'smth'} click={clickFuncMock}/>);
        const button = getByText('smth');
        expect(clickFuncMock).not.toHaveBeenCalled();
        button.click();
        expect(clickFuncMock).toHaveBeenCalled();
    })
});