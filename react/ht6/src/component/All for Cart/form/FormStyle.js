import styled from "styled-components";
const DefaultInput = styled.input`
        width: 45vw;
        height: 50px;
        padding: 0 8px;
        font-size: 12pt;
        background: black;
        color: yellow;
        border: 0;
        letter-spacing: 1px;
        cursor: pointer;
        border-right: 3px solid yellow;
        border-left: 3px solid yellow;
        border-bottom: 3px solid yellow;
        `;
const Header = styled.h3`
        box-sizing: border-box;
        font-size: 16pt;
        letter-spacing: 2px;
        font-weight: 500;
        width: 100%;
        color: yellow;
        text-transform: uppercase;
        background: black;
        text-align: center;
        padding: 20px 0;
        border: 3px solid yellow;
        `;
const Submit = styled.button`
        box-sizing: border-box;
        font-size: 16pt;
        letter-spacing: 2px;
        font-weight: 500;
        width: 100%;
        color: yellow;
        text-transform: uppercase;
        background: black;
        text-align: center;
        padding: 20px 0;
        border-top: 0;
        border-right: 3px solid yellow;
        border-left: 3px solid yellow;
        border-bottom: 3px solid yellow;
        `;
const Error = styled.div`
        color: #E74C3C;
        border-bottom: 3px solid yellow;
        text-transform: uppercase;
        background: black;
        text-align: center;
        padding: 10px 0;
        box-sizing: border-box;
        font-size: 16pt;
        border-right: 3px solid yellow;
        border-left: 3px solid yellow;
        `;

export {
        Header,
        DefaultInput,
        Submit,
        Error
};