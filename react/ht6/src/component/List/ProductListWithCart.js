import React, {useState} from 'react';
import Card from "../Card/Card";
import FormikForm from "../All for Cart/form/form";
import {connect} from "react-redux";
import CartButton from '../All for Cart/Cart button/CartButtonStyle'
import {GridContainer} from "../../Center";
const ProductListWithCart = (props) => {
    const [formIsOpen, openForm] = useState(false);
    const cartHaveSmth= !!props.filter.length;
    return (
        //Проверяем наличие товара в корзине и прогружаем товар и кнопку оформить
        <>
            {cartHaveSmth && <CartButton onClick={() => openForm(true)} data-testid='buttonForm'>Open Cart</CartButton> }
            {formIsOpen && <FormikForm data-testid='formikForm' workWithWindow={openForm} cart={props.cart} sold={props.sold} />}
            <GridContainer data-testid='listWithCart'>
                {props.filter.map(elem => (
                    <Card key={elem.VendorCode}
                          bg={elem.URL} name={elem.Name}
                          id={elem.VendorCode}
                          color={elem.Color} price={elem.Price}/>
                ))}
            </GridContainer>
        </>
    )
};
const mapStateToProps = ({ card }) => ({
    cart: card.cart,
});
const mapDispatchToProps = (dispatch) => ({
    sold: () => dispatch({type: "SOLD"})
});
export default connect(mapStateToProps, mapDispatchToProps)(ProductListWithCart)