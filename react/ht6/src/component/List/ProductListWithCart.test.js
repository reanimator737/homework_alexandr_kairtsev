import ProductListWithCart from './ProductListWithCart'
import thunk from "redux-thunk";
import configureMockStore from "redux-mock-store";
import {render, screen} from "@testing-library/react";
import {Provider} from "react-redux";
import React from "react";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
function createPropsAndStore(data,favorite, cart){
    return {
        props: {
            "name" : "Samsung Galaxy S20",
            "price" : "640$",
            "bg" : "SamsungGalaxyS20.jpg",
            "color" : "Navy",
            "id": '1111'
        },
        store: mockStore({
            card: {
                favorite: favorite || [],
                cart: cart || []
            },
            loadData: {
                data: data || [],
                isLoading: false
            }
        })
    }
}
describe('Test window with /cart', () => {
   test('Smoke test', () => {
       const {store, props} = createPropsAndStore();
       const {container} = render(<Provider store={store}><ProductListWithCart {...props} filter={[]}/></Provider>);
       //Обертка отрендерится даже без карточек (container.children[0] - это обертка)
       expect(container.children.length).toBe(1);
       expect(container.children[0].tagName).toBe('DIV');
       expect(container.children[0].children.length).toBe(0);
   });
    test('Test with props.filter', () =>{
        const {store, props} = createPropsAndStore();
        const {container} = render(<Provider store={store}><ProductListWithCart {...props} filter={[{VendorCode: 1111}]}/></Provider>);
        expect(container.children.length).toBe(2);
        //Кнопка открытия формы
        expect(container.children[0].tagName).toBe('BUTTON');
        expect(container.children[1].tagName).toBe('DIV');
        expect(container.children[1].children.length).toBe(1);
        //Заодно проверка открытия формы
        //children[0] - кнопка для открытия формы
        container.children[0].click();
        screen.getByText('Registration form');
    })
});