import React from 'react';
import Card from "../Card/Card";
import {GridContainer} from "../../Center";
const ProductList = (props) => {
  return (
      //Рендер отфильтрованного списка элементов.
          <GridContainer data-testid='list'>
              {props.filter.map(elem => (
                  <Card key={elem.VendorCode}
                        bg={elem.URL} name={elem.Name}
                        id={elem.VendorCode}
                        color={elem.Color} price={elem.Price}/>
                  ))}
          </GridContainer>
  )
};
export default ProductList