import reducer from "./reducer";
describe('Test work with cart', ()=>{
    test('ADD_CART reducer', () => {
        const TEST_ELEMENT = [{id: 1}];
        const initialState = {
            favorite: [],
            cart: []
        };
        const action = {
            type: 'ADD_CART',
            payload: TEST_ELEMENT
        };
        const newState = reducer(initialState, action);
        expect(newState.cart).toBe(TEST_ELEMENT);
        expect(newState.favorite).toBe(initialState.favorite);
    });
    test('REMOVE_CART reducer', () => {
        const TEST_ELEMENT = [{id: 1}];
        const initialState = {
            favorite: [],
            cart: TEST_ELEMENT
        };
        const action = {
            type: 'REMOVE_CART',
            payload: []
        };
        const newState = reducer(initialState, action);
        expect(newState.cart).toBe(action.payload);
        expect(newState.favorite).toBe(initialState.favorite);
    });
    test('SOLD reducer', () => {
        const TEST_ELEMENT = [{id: 1}, {id: 2}, {id: 3}];
        const initialState = {
            favorite: [],
            cart: TEST_ELEMENT
        };
        const action = {
            type: 'SOLD'
        };
        const newState = reducer(initialState, action);
        expect(newState.cart).toEqual([]);
        expect(newState.favorite).toBe(initialState.favorite);
    });

});
