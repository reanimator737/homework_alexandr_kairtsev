import {findAndCut, pushToState} from "./actions";
describe('Test action in card', ()=>{
    test('Test findAndCut func length > 0', () => {
        const TEST_ARRAY = [{id: 1}, {id: 2}, {id: 3}];
        const TEST_INDEX = 1;
        const newState = findAndCut(TEST_ARRAY, TEST_INDEX);
        expect(newState).toEqual([{id: 1},{id: 3}]);
    });
    test('Test findAndCut func length = 0', () => {
        const TEST_ARRAY = [{id: 1}];
        const TEST_INDEX = 0;
        const newState = findAndCut(TEST_ARRAY, TEST_INDEX);
        expect(newState.length).toBe(0);
    });
    test('Test pushToState', () => {
        const TEST_ARRAY = [{id: 1}];
        const TEST_ELEM = {id: 2};
        const newState = pushToState(TEST_ARRAY, TEST_ELEM);
        expect(newState).toEqual([{id: 1}, {id: 2}]);
    });
});
