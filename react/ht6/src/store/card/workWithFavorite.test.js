import reducer from "./reducer";
describe('Test work with favorite', ()=>{
    test('ADD_FAVORITE reducer', () => {
        const TEST_ELEMENT = [{id: 1}];
        const initialState = {
            favorite: [],
            cart: []
        };
        const action = {
            type: 'ADD_FAVORITE',
            payload: TEST_ELEMENT
        };
        const newState = reducer(initialState, action);
        expect(newState.favorite).toBe(TEST_ELEMENT);
        expect(newState.cart).toBe(initialState.cart);
    });
    test('REMOVE_FAVORITE reducer', () => {
        const TEST_ELEMENT = [{id: 1}];
        const initialState = {
            favorite: TEST_ELEMENT,
            cart: []
        };
        const action = {
            type: 'REMOVE_FAVORITE',
            payload: []
        };
        const newState = reducer(initialState, action);
        expect(newState.favorite).toBe(action.payload);
        expect(newState.cart).toBe(initialState.cart);
    });

});