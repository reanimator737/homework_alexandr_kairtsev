export const dataLoading = (isLoading) => ({
    type: 'DATA_LOADING',
    payload: isLoading
});

export const saveData = (data) => ({
    type: 'SAVE_DATA',
    payload: data
});