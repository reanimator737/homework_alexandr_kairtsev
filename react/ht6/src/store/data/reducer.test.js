import reducer from "./reducer";
describe('Test saving data from json', ()=>{
   test('Loading data', () => {
      const initialState = {
         data: [],
         isLoading: false
      };
      const action = {
         type: 'DATA_LOADING',
         payload: true
      };
     const newState = reducer(initialState, action);
     expect(newState.data).toBe(initialState.data);
     expect(newState.isLoading).toBe(action.payload);
   });
   test('Saving data from json', () =>{
      const TEST_DATA = [{}, {}, {}];
      const initialState = {
         data: [],
         isLoading: true
      };
      const action ={
         type: 'SAVE_DATA',
         payload: TEST_DATA
      };
      const newState = reducer(initialState, action);
      expect(newState.data).toBe(action.payload);
      expect(newState.isLoading).toBe(initialState.isLoading);
   });
});