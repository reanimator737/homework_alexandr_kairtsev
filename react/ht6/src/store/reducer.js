import {combineReducers} from "redux";
import loadData from './data/reducer'
import card from './card/reducer'
export const reducer = combineReducers({
    loadData,
    card,
});